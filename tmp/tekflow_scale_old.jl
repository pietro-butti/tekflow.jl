mutable struct TEKflowset
    flows::Vector{TEKflow}
    t0s::Vector{Measurement}
    Nsets::Int64
    function TEKflowset(flows,t0s)
        this = new(flows,t0s)
        this.Nsets = length(flows)
        return this
    end
end

mutable struct Parameters
    Tmin::Float64
    gamma::Float64
    Npars::Int64
end
tmax(γ,Ncol) = γ^2*Ncol/8
alpha = (3. /128. /π^2)


b₀(Nf) = (11. - 4. *Nf)/3. /8. / π^2
b₁(Nf) = (34. - 32. *Nf)/3. /128. / π^4
alpha = 3. /128. / π^2

mutable struct flat_data
    flat_time::Vector{Float64}
    flat_ϕ::Vector{uwreal}
    flat_iifit::Vector{Int64}
    flat_t0ii::Vector{Int64}
end

mutable struct TEKflat
    flows::TEKflowset
    param::Parameters
    fdata::flat_data

    function TEKflat(_fs::TEKflowset,_ps::Parameters)
        this = new(_fs,_ps)

        iis   = Vector{Vector{Int64}}(undef,_fs.Nsets)
        ϕs    = Vector{Vector{uwreal}}(undef,_fs.Nsets)
        times = Vector{Vector{Float64}}(undef,_fs.Nsets)
        fitr  = Vector{Vector{Int64}}(undef,_fs.Nsets)
        t0ii  = Vector{Vector{Int64}}(undef,_fs.Nsets)

        for (ii,flow) in enumerate(_fs.flows)
            iifit = [i for (i,el) in enumerate(flow.time) if el>_ps.Tmin && el<tmax(_ps.gamma,flow.N)]

            times[ii] = flow.time[iifit]
            ϕs[ii]    = flow.t2Euw[iifit]
            fitr[ii]  = iifit
            t0ii[ii]  = fill(ii,length(iifit))
            iis[ii]   = collect(1:50) .+ length(iifit)*(ii-1)
        end

        # Flatten datas
        flat_time  = vcat(times...)
        flat_ϕ     = vcat(ϕs...)
        flat_iifit = vcat(fitr...)
        flat_t0ii  = vcat(t0ii...)

        this.fdata = flat_data(flat_time, flat_ϕ, flat_iifit, flat_t0ii)

        return this
    end
end




function flat_fit(F::TEKflat,p0s,ref,Nf; verbose=true)
    flatd  = F.fdata
    params = F.param

    flat_err = (alpha./val.(flatd.flat_ϕ).^2 .- b₁(Nf)/b₀(Nf)./val.(flatd.flat_ϕ)).*err.(flatd.flat_ϕ)
    function flat_model(data,pars::Vector{Float64})
        aux = Vector{eltype(data)}(undef,length(data))
        for (i,f) in enumerate(data)
            aux[i] = b₀(Nf)/2*pars[params.Npars+flatd.flat_t0ii[i]] -
                (alpha/f-alpha/ref) - 
                b₁(Nf)/b₀(Nf)*log(f/ref) -
                sum([pars[k]/k/alpha^k*(f^k-ref^k) for k in 1:params.Npars])
        end
        return aux
    end

    fit = curve_fit(
        flat_model,
        val.(flatd.flat_ϕ),
        b₀(Nf)/2 .* log.(flatd.flat_time),
        1.0 ./ flat_err.^2,
        p0s
    )

    if verbose
        try
            parameters = coef(fit) .± stderror(fit)
            println(
            "--------- FIT RESULTS (w/ stat. error) --------- \n",
            "PT coefficients:",
            )
            [println("                 $(parameters[k])") for k in 1:params.Npars];
            println("sqrt(8t₁):")
            [println("$(F.flows.flows[i].basement) => $(sqrt(8*exp(parameters[params.Npars+i]))) ") for i in 1:F.flows.Nsets];
            println(
                "\n",
                "χ²/(Ndof=$(dof(fit))) = $(round(sum(fit.resid.^2)/dof(fit),digits=3))\n",
                "---------------------------------------------"
            )
        catch
            println("ROUTINE ERROR: could not produce interface mask")
        end
    end

    return fit
end



# Error analysis with ADerrors
function stat_ADerr(F::TEKflat,fit,ref,Nf; verbose=true)
    β(λ,Nf) = -b₀(Nf)*λ^2 / (1 - b₁(Nf)/b₀(Nf)*λ)

    param = F.param
    flatd = F.fdata

    params_guess = [
        coef(fit)[1:param.Npars]
        exp.(coef(fit)[param.Npars+1:end])
    ]

    ## Define a cost function and minimize
    δλ = err.(flatd.flat_ϕ)./alpha
    # Base.abs(uw::uwreal) = Base.abs(uw.mean)
    function cost(p,data)
        Hₜ = [
                alpha/f - alpha/ref +
                b₁(Nf)/b₀(Nf)*log(f/ref) + 
                sum( [p[k]/k/alpha^k*(f^k - ref^k) for k in 1:param.Npars] ) +
                b₀(Nf)/2*log(flatd.flat_time[i]/exp(p[param.Npars+flatd.flat_t0ii[i]]))
            for (i,f) in enumerate(data)
        ]
        δHₜ = b₀(Nf) ./ δλ ./ abs.(β.(data./alpha,Nf))
        return sum((Hₜ./δHₜ).^2)
    end

    # Propagate error
    @time (fitp,χexp) = fit_error(cost,coef(fit),flatd.flat_ϕ)
    uwerr.(fitp)

    if verbose
        try
            println(
                "--------- FIT RESULTS (w/ stat. error (ADerrors)) --------- \n",
                "PT coefficients:",
            )
            [println("                 $(fitp[k])") for k in 1:param.Npars];
            println("sqrt(8t₁):")
            st = sqrt.(8 .*exp.(fitp)); uwerr.(st)
            [println("$(flows[i][2].basement) => $(st[param.Npars+i]) ") for i in 1:param.Nsets];
            println(
                "\n",
                "χ²/(Ndof=$(dof(fit))) = $(round(sum(fit.resid.^2)/dof(fit),digits=3))\n",
                "---------------------------------------------"
            )
        catch
            println("ROUTINE ERROR: could not produce interface mask")
        end
    end

    return fitp,χexp
end