# mutable struct gfit
#     parameters::Vector{uwreal}

#     χ::Float64
#     χdof::Float64
#     χexp::Float64

#     function gfit()
#         this = new()
#         return this
#     end
# end

mutable struct fitpar
    Nshared::Int64
    Npersonal::Int64
    shared::Vector{Float64}
    personal::Vector{Float64}
    χ::Float64

    function fitpar(nsh::Int64,nper::Int64)
        this = new()

        this.Nshared = nsh
        this.Npersonal = nper

        this.shared   = Vector{Float64}(undef,this.Nshared)
        this.personal = Vector{Float64}(undef,this.Npersonal)

        return this
    end
end


"""
globalfit(model::Function, Xdata::Vector{T}, Ydata::Vector{T}, Npersonal::Int64, Nshared::Int64; fixedpars=Vector{Float64}([]), log=true) where T

This function performs simultaneous fit of different dataset to a model with some shared parameters.

### ARGUMENTS 
- `model` is a function which takes 2 arguments model(x,pars), where pars = [pars[<personal>],fixedpars,pars[<shared>]]
- `Xdata` and `Ydata` are vectors of  an unspecified type T. We refer to the inner vectors as "dataset". The type T will presumably be Vector{Float64} or Vector{uwreal}
- `Npersonal` number of fit parameters which is left free for each dataset
- `Nshared` number of fit parameters which have to be common for each dataset
### OPTIONAL ARGUMENTS
- `fixedpars=Vector{Float64}([])` is a vector containing parameters in the model that do not have to be fitted.
    e.g. let's say that 2 datasets have to be fitted to 1 personal parameter
                                                        2 parameter has to be = `[27,289]`(1st dataset), `[12,435]`(2nd dataset)  
                                                        2 shared parameters
        `model(x,[pars[1],[27,289],pars[7]])`
        `model(x,[pars[2],[12,435],pars[8]])`
    then we have to feed globalfit 
        `fixedpars = [27,289,12,435]`
- `log=true` prints information about results and timing

E.g.  We have 4 dataset with Nshared=2, Ncommon=3 ===> in total we want to extract 11(= Nsets*Npersonal + Nshared) parameters
    1st dataset ---> model( Xdata[1], [pars[1],pars[2],pars[9],pars[10],pars[11]] ) 
    2nd dataset ---> model( Xdata[2], [pars[3],pars[4],pars[9],pars[10],pars[11]] ) 
    3rd dataset ---> model( Xdata[3], [pars[5],pars[6],pars[9],pars[10],pars[11]] ) 
    4th dataset ---> model( Xdata[4], [pars[7],pars[8],pars[9],pars[10],pars[11]] ) 
"""    
function globalfit(model::Function, Xdata::Vector{Tx}, Ydata::Vector{Ty}, Npersonal::Int64, Nshared::Int64; fixedpars=Vector{Float64}([]), p0=Vector{Float64}([]), log=true) where Tx where Ty
    tic = time()

    # SANITY CHECK FOR DIMENSIONS
    if size(Xdata,1)!=size(Ydata,1)
        error("LOCAL ERROR (1): X data and Y data do not have the same size. Returning -1")
    else
        Nset = size(Xdata)[1]
        for (set,(x,y)) in enumerate(zip(Xdata,Ydata))
            if size(x,1)!=size(y,1)
                error("LOCAL ERROR (2): X data and Y data in set n. $set does not have the same dimension. Returning -1...")
            end
        end
    end

    # Check fixed pars dimension
    if length(fixedpars)%Nset!=0
        error("LOCAL ERROR: fixedpars dimension is not compatible. Return -1...")
    else
        Nfixed_per_set = Int64(length(fixedpars)/Nset)
    end

    Npars = Npersonal+Nshared+Nfixed_per_set
    # Flatmodel takes model::Function and distributed over flatten vector
    function flatmodel(flatT::Vector{Float64},pars::Vector{Float64})
        Y = Vector{Float64}(undef,length(flatT))
        tmin = 1
        for set in 1:Nset
            tmax = (tmin-1) + length(Xdata[set])
            
            # thispars = [pars[<personal>],fixedpars,pars[<shared>]]
            fixed = fixedpars[(set-1)*Nfixed_per_set+1:set*Nfixed_per_set]
            thispars = vcat(pars[set*Npersonal-Npersonal+1:set*Npersonal],fixed,pars[end-Nshared+1:end])
            aux = model(flatT[tmin:tmax],thispars)
            
            Y[tmin:tmax] = aux[:]

            tmin = tmax + 1
        end
        return Y
    end
    
    # Produce a p0 vector
    if length(p0)==0
        P0 = fill(0.5,Int64(Nset*Npersonal + Nshared))
    elseif length(p0)<Int64(Nset*Npersonal + Nshared)
        println("USER ERROR: p0 length has to be $(Int64(Nset*Npersonal + Nshared)). Returning -1.0")
        return -1.0
    else
        P0 = p0
    end


    # Flatten data
    flatX, flatY = vcat(Xdata...), vcat(Ydata...)


    # curve_fit needs Vector{Float64} to work. Check that    
    fitp = 0
    if typeof(flatY[1])==Float64
        fit = curve_fit(flatmodel,flatX,flatY,P0)
        fitp = coef(fit)
        χ = sum(fit.resid.^2)
    elseif typeof(flatY[1])==uwreal
        fit = curve_fit(flatmodel,flatX,value.(flatY),1.0 ./ err.(flatY) .^2 , P0)
        (fitp,cexp) = fit_error(flatchisq,coef(fit),flatY)
        uwerr.(fitp)
        χ = sum(fit.resid.^2 ./ err.(flatY).^2)
    else
        error("LOCAL ERROR: I cannot fit this kind of data because curve_fit does not take $(typeof(flatY)) as input. Please, check the tipe of Xdata and Ydata. Returning -1...")
    end


    tac = time()

    fitg = fitpar(Nshared,Npersonal*Nset)
    fitg.personal = fitp[1:Npersonal*Nset]
    fitg.shared = fitp[end-Nshared+1:end]
    fitg.χ = χ

    if log
        println("================= Simultaneous fit results ===================")
        println("$Nset sets of data fitted to $model")
        println("Personal parameters ($Npersonal each): ")
        for set in 1:Nset
            for pers in 1:Npersonal 
                which = set*Npersonal-Npersonal+1 + (pers-1)
                println("Set n. $set ---> parameter[$which] = $(fitp[which])")
                # push!(fitg.personal,fitp[which])
            end
        end
        println("Common parameters ($Nshared in total): ")
        for com in 1:Nshared
            println("          ---> parameter[$com] = $(fitp[Nset*Npersonal+com])")
            # push!(fitg.shared,fitp[Nset*Npersonal+com])
        end
        println("χ² = $χ")
        println("χ²/dof = χ²/($(length(flatX)) - $((Nset*Npersonal+Nshared))) $(χ/(length(flatX)-(Nset*Npersonal+Nshared)))")
        println("---------------------------------------------------------------")
        println("Function timing: $(tac-tic)")
        println("===============================================================")
    end

    return fitg

    
end
    









































"""
    χ2(model::Function,xdata::Vector{T1},ydata::Vector{uwreal},parameters::Vector{T2}) where T1 where T2

This function calculates the χ² for some data set and some model. If `xdata` points have an error they are added in quadrature to the error in `ydata`
"""
function χ2(model::Function,xdata::Vector{T1},ydata::Vector{uwreal},parameters::Vector{T2}) where T1 where T2
    if T2==uwreal
        pars=value.(parameters)
    else
        pars=parameters
    end

    yerr = err.(ydata)
    if T1==uwreal
        xx   = value.(xdata)
        yerr = value.(ydata) .* sqrt.( (err.(ydata)/value.(ydata)).^2 .+ (err.(xdata)/value.(xdata)).^2 )
    else
        xx = xdata
        yerr = err.(ydata)
    end

    return sum(( value.(ydata) .- model(xx,pars) ) .^ 2 ./ yerr .^ 2)
end



function curvefit(model::Function,xdata::Vector{Float64},ydata::Vector{T},p0::Vector{Float64}; weights=true, yerr=Vector{Float64}([])) where T
    if T==uwreal
        Y = value.(yerr)
    elseif T==Float64
        Y = ydata
    end

    if weights && T==uwreal
        return curve_fit(model,xdata,Y,1.0 ./ err.(ydata).^2,p0)
    elseif weights && T!=uwreal && length(yerr)!=0
        return curve_fit(model,xdata,Y,1.0 ./ yerr.^2,p0)
    elseif T!=uwreal
        return curve_fit(model,xdata,ydata,p0)
    end
        
end
