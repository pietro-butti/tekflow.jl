using PyPlot, tekflow, JLD2





flows = [
    "n841b0350k9",
    "n841b0355k9",
    "n841b0385k9",
    "n841b0390k9"
]
path = "/Users/pietro/Data/flow/processed"

flow = TEKflow("$(flows[3])o100",path="/Users/pietro/Data/flow/nf0")
PTcorrect!(flow)


T = flow.time[20:100]
ϕ = flow.t2Euw[20:100]



b₀(nf) = (11. - 4. *nf)/3. /8. / π^2
b₁(nf) = (34. - 32. *nf)/3. /128. / π^4
α = 3. /128. / π^2

yfit = log.(T)
xfit = val.(ϕ)

erry = α./ϕ.^2 - 



ref = 0.05
Npars = 3

function model(ff,p)
    aux = deepcopy(ff) .* 0. 
    for (i,f) in enumerate(ff)
        aux[i] = p[end] + 
            2/b₀(p[end-1])*(1/f - 1/ref) + 
            - 2*b₁(p[end-1])/b₀(p[end-1])^2 * log(f/ref) + 
            - sum([2*p[k]/b₀(p[end-1])/k * ((f/α)^k - (ref/α)^k ) for k in 1:Npars])
    end
    return aux
end


fit = curve_fit(model,)