using PyPlot, tekflow, JLD2, Polynomials





flows = [
    "n841b0350k9",
    "n841b0355k9",
    "n841b0385k9",
    "n841b0390k9"
]
path = "/Users/pietro/Data/flow/processed"

flow = TEKflow("$(flows[3])o100",path="/Users/pietro/Data/flow/nf0")
PTcorrect!(flow)







function ciao(time,ϕ,refval; Tmin=1.0,Tmax=4.0,t2Emax=0.12, Npar=3, deltat=0.03125)
    # Filter data in cuts
    iicut = [tt for (tt,el) in enumerate(ϕ) if ( deltat*tt>Tmin && deltat*tt<Tmax && el<t2Emax )]
    flowplot(time[iicut],ϕ[iicut])
    @. model(x,p) = Polynomial(p)(x)






    # χ2(p,data) = sum( (data .- model(time[iicut],p).^2 ./ err.(ϕ[iicut])) .^ 2)


end

N = 841
Tmin = 2.5
γ = 0.26
Tmax = γ^2*N/8
ciao(flow.time,flow.t2Euw,0.05,Tmin=Tmin,Tmax=Tmax)


behold()