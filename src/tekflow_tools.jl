"""
    rebin(array::Vector{T};Nbin=-1,binsize=-1) where T

This function rebin a vector
"""
function rebin(array::Vector{T};Nbin=-1,binsize=-1) where T
    if (Nbin==-1 && binsize==-1)
        println("Missing shaping parameters (Nbin or binsize). Returning -1")
        aux = -1
    elseif (Nbin!=-1 && binsize!=-1)
        println("Choose just one shaping parameters (Nbin or binsize). Returning -1")
        aux = -1
    elseif (Nbin==0 || binsize==0)
        println("0 is not a valid number. Returning -1")
        aux = -1
    else
        if Nbin!=-1
            delta = Nbin
            axes = 2
        elseif binsize!=-1
            delta = binsize
            axes = 1
        end
        aux = vec(mean(reshape(array[1:length(array)÷delta*delta],delta,:),dims=axes))
    end
    
    return aux
end




function average_creutz(flow::TEKflow,saveto::String)
    if flow.flag_multicol_has_been_called == false
        error("Call first multicol. Returning -1...")
    end

    f = open(saveto,"w")
    @printf(f,
        "# t            χ(1.5)                          χ(2.5)                          χ(3.5)                          χ(4.5)                          χ(5.5)\n"
    )
    for tt in 1:flow.tmax
        @printf(f,
            "%.8f     %.10f  %.10f      %.10f  %.10f      %.10f  %.10f      %.10f  %.10f      %.10f  %.10f\n",
            flow.time[tt],
            value(flow.multi[1][tt]),err(flow.multi[1][tt]),
            value(flow.multi[2][tt]),err(flow.multi[2][tt]),
            value(flow.multi[3][tt]),err(flow.multi[3][tt]),
            value(flow.multi[4][tt]),err(flow.multi[4][tt]),
            value(flow.multi[5][tt]),err(flow.multi[5][tt]),
        ) 
    end
    close(f)

    return 0
end

function average(flow::TEKflow,saveto::String)
    f = open(saveto,"w")
    if flow.flag_tddt_flow_has_been_called
        @printf(f,
            "# t            t²E(t)           σ             t*d(t²E(t))/dt           σ \n"
        )
        for tt in 1:flow.tmax
            @printf(f,
                "%.8f     %.10f  %.10f      %.10f  %.10f \n",
                flow.time[tt],value(flow.t2Euw[tt]),err(flow.t2Euw[tt]),value(flow.wt2Euw[tt]),err(flow.wt2Euw[tt])
            ) 
        end    
    else
        @printf(f,
            "# t            t²E(t)           σ \n"
        )
        for tt in 1:flow.tmax
            @printf(f,
                "%.8f     %.10f  %.10f \n",
                flow.time[tt],value(flow.t2Euw[tt]),err(flow.t2Euw[tt])
            ) 
        end
    end
    close(f)

    return 0
end


# for file in $(ls /home/pietro/Data/creutz/n289b0340k5hf1850/); do nlines=$(cat /home/pietro/Data/creutz/n289b0340k5hf1850/$file | wc -l); if [ "$nlines" -lt 209 ]; then echo $file;fi ;done

