"""
        tddt_flow(t2E::Vector{Float64},deltat::Float64)

        tddt_flow(flow::TEKflow)

        tddt_flow(t2E::Vector{uwreal},deltat::Float64)

    This function calculates the derivative of the flow as
        t d/dt (t^2 E(t))
    which is given as an ouptut.
    Derivative is calculated as a 4th order-improved finite difference as 
        `f'[i] = (f[i-2] - 8*f[i-1] + 8*f[i+1] - f[i+2])/(12*h)`

    Multiple dispached version takes as an input
    - `t2E::Vector{Float64}` : a vector of `Float64`
    - `flow::TEKflow`        : a TEKflow struct
    - `t2E::Vector{uwreal}`  : a vector of `uwreal` if you want to use ADerrors
"""
function tddt_flow(t2E::Vector{Float64},deltat::Float64)
    factor = 1. /12. / deltat
    ddt = Vector{Float64}(undef,length(t2E)) 

    for tt in 1:length(t2E)
        if (tt==1 || tt==2 || tt==length(t2E)-1 || tt==length(t2E))
            ddt[tt] = 0.
        else
            ddt[tt] = factor*(t2E[tt-2] - 8. *t2E[tt-1] + 8. *t2E[tt+1] - t2E[tt+2])
        end
        ddt[tt] *= deltat*tt
    end
    
    return ddt
end
function tddt_flow(t2E::Vector{uwreal},deltat::Float64)

    factor = 1. /12. / deltat
    ddt = Vector{uwreal}(undef,length(t2E))
    for tt in 1:length(t2E)
        if (tt==1 || tt==2 || tt==length(t2E)-1 || tt==length(t2E))
            ddt[tt] = uwreal([0.,0.],"default")
        else
            ddt[tt] = factor*(t2E[tt-2] - 8. *t2E[tt-1] + 8. *t2E[tt+1] - t2E[tt+2])
        end
        ddt[tt] *= deltat*tt
        uwerr(ddt[tt])
    end
    
    return ddt
end
function tddt_flow(flow::TEKflow)
    flow.flag_tddt_flow_has_been_called = true
    flow.wt2E = Vector{Float64}(undef,flow.tmax)
    flow.wt2Edata = Matrix{Float64}(undef,flow.Nconf,flow.tmax)
    flow.wt2Euw = Vector{uwreal}(undef,flow.tmax)

    factor = 1. /12. / flow.deltat

    # Calculate data derivatives
    for conf in 1:flow.Nconf
        for tt in 1:flow.tmax
            if (tt==1 || tt==2 || tt==flow.tmax-1 || tt==flow.tmax)
                flow.wt2Edata[conf,tt] = 0.
            else
                flow.wt2Edata[conf,tt] = flow.deltat*tt*factor*(flow.t2Edata[conf,tt-2] - 8. *flow.t2Edata[conf,tt-1] + 8. *flow.t2Edata[conf,tt+1] - flow.t2Edata[conf,tt+2])
            end
        end
    end

    error = flow.deltat^4/30.
    uwfactor = uwreal([0.,error],"4th order derivative error")
    # Calculate derivatives of main vectors
    for tt in 1:flow.tmax
        if (tt==1 || tt==2 || tt==flow.tmax-1 || tt==flow.tmax)
            flow.wt2E[tt] = 0.
            flow.wt2Euw[tt] = uwreal([0.,error],"$(flow.basement) flow derivative")
        else
            flow.wt2E[tt]   = factor*(flow.t2E[tt-2] - 8. *flow.t2E[tt-1] + 8. *flow.t2E[tt+1] - flow.t2E[tt+2])
            flow.wt2Euw[tt] = factor*(flow.t2Euw[tt-2] - 8. *flow.t2Euw[tt-1] + 8. *flow.t2Euw[tt+1] - flow.t2Euw[tt+2]) + uwfactor
            
            flow.wt2E[tt] *= flow.deltat*tt
            flow.wt2Euw[tt] *= flow.deltat*tt
        end
        uwerr(flow.wt2Euw[tt])
    end

    return
end