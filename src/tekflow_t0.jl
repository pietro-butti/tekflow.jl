# function find_t0(t2E::Vector{Float64},refval::Float64;delta=0.01,deltat=0.03125,time_array=0)
    
#     if time_array==0
#         time = [tii*deltat for tii in 1:length(t2E)]
#     elseif typeof(time_array)==Vector{Float64}
#         if length(time_array)!=length(t2E)
#             println("USER ERROR: time and t2E array are not of the same length. Returning -1...")
#             return -1
#         else
#             time = time_array
#         end
#     end


#     # Filter data in a proper range
#     iifit = [i for (i,el) in enumerate(t2E) if ( el>refval-delta && el<refval+delta )]

#     # Check if there is enough data and filtered values are monotonous
#     flag = false
#     if length(t2E[iifit])>5
#         prima = t2E[iifit][1]
#         for el in t2E[iifit][2:end]
#             dt = el-prima
#             if dt<0
#                 flag = true
#                 break
#             end
#             prima = el
#         end
#     else
#         flag = true
#     end
#     if flag==true
#         println("Flow is not ok. Take care of him, maybe plot it and see.")
#         return undef
#     end

    
#     # Interpolate
#     @. model(x,par) = par[1]*x + par[2]
#     t0 = -100
#     try
#         fit = curve_fit(model,time[iifit],t2E[iifit],[.5,.5])
#         t0 = (refval - coef(fit)[2])/coef(fit)[1]
#     catch
#         println("Something went wrong, do not trust t0.")
#     end

#     return t0
# end
# function find_t0(t2E::Vector{T},refval::Float64;delta=0.01,deltat=0.03125,time_array=0) where T
#     if time_array==0
#         time = [tii*deltat for tii in 1:length(t2E)]
#     elseif typeof(time_array)==Vector{Float64}
#         if length(time_array)!=length(t2E)
#             println("USER ERROR: time and t2E array are not of the same length. Returning -1...")
#             return -1
#         else
#             time = time_array
#         end
#     end

#     # Filter data in a proper range
#     iifit = [i for (i,el) in enumerate(val.(t2E)) if ( el>refval-delta && el<refval+delta )]

#     # Check if resulting data are fit-able
#     flag = false
#     if length(t2E[iifit])>5
#         prima = t2E[iifit][1]
#         for el in t2E[iifit][2:end]
#             dt = el-prima
#             if dt<0
#                 flag = true
#                 break
#             end
#             prima = el
#         end
#     else
#         flag = true
#     end
#     if flag==true
#         println("Flow is not ok. Take care of him, maybe plot it and see.")
#         return undef
#     end

#     @. model(x,par) = par[1]*x + par[2]
#     fit = curve_fit(model, time[iifit], val.(t2E[iifit]), 1.0 ./ err.(t2E[iifit]) .^2, [.5,.5])
#     # t0 = (-coef(fit)[2] + sqrt(coef(fit)[2]^2 - 4*coef(fit)[1]*(coef(fit)[3]-refval)))/2/coef(fit)[1]
#     t0 = (refval-coef(fit)[2])/coef(fit)[1]

#     if T==Measurement
#         p = coef(fit) .± stderror(fit)
#         t0 = (-p[2] + sqrt(p[2]^2 - 4*p[1]*(p[3]-refval)))/2/p[1]
#     elseif T==uwreal
#         # Define linear model and chisq function needed by ADerrors
#         @. model(x,par) = par[1]*x + par[2]
#         chisq(par, data) = sum( (data .- model(time[iifit],par)) .^2 ./ 0.01 .^ 2 )

#         # Fit data
#         fit = curve_fit(model, time[iifit], value.(t2E[iifit]), 1.0 ./ err.(t2E[iifit]) .^2, [.5,.5])
#         (fitpar, cexp) = fit_error(chisq, coef(fit), t2E[iifit])
#         uwerr.(fitpar)

#         t0 = (refval - fitpar[2])/fitpar[1]
#         uwerr(t0)
#     end

#     return t0
# end
"""
    find_t0_interpolate(time::Vector{Float64}, ϕ::Vector{T}, refval::Float64; delta=0.005) where T

This function extract a reference scale given: a vector `Float64` or `uwreal`, a reference value where to cut the flow curve. 

### Returned value
The returned value is that `t₀` that solve the equation
    < t₀²E(t₀) > = refval

### Methodology

- _Data filtering_: only those points in `ϕ` which are between `refval-delta` and `refval+delta` are kept. (delta is set by default to 0.01)
- _Sanity check_: remaining points are more than 5 and if there is no bending. `undef` is returned otherwise
- _Interpolation_: Remaining points are fitted to a straight line, and t0 is given solving the linear equation.
"""
function find_t0_interpolate(time::Vector{Float64}, ϕ::Vector{T}, refval::Float64; delta=0.005) where T
    deltat = time[2]-time[1]

    ## Data filtering_
    iiok = [i for (i,phi) in enumerate(ϕ) if (val(phi)>(refval-delta) && val(phi)<(refval+delta))]
    steps = unique([time[iiok[i+1]]-time[iok] for (i,iok) in enumerate(iiok[1:end-1])])

    ## Sanity check
    if length(iiok)<5 
        println("Too few value in refval±delta. Maybe enlarge delta")
        return undef
    elseif length(steps)>1 
        println("Probably there is a bending. Maybe pre-filter flow curve") 
        return undef
    end


    error = 0
    try
        error = tekflow.err.(ϕ[iiok])
    catch
        println("Error assigned .01 to every point")
        error = fill(0.01,length(ϕ[iiok]))
    end

    # Fit with parabola
    @. model(x,p) = p[1] + p[2]*x
    fit = curve_fit(model,time[iiok],val.(ϕ[iiok]), 1.0 ./ error.^2, [.5,.5])

    (q,m) = coef(fit) .± stderror(fit)
    t0 = (q-refval)/m

    return t0
end






"""
    find_t0_extrapolate(time::Vector{Float64},t2E::Vector{uwreal},refval::Float64; delta=0.01,deltat=0.03125, Tmin=1.0,Tmax=4.0,t2Emax=0.12, verbose=false)

This function extract a reference scale given: a vector of `uwreal`, a reference value where to cut the flow curve.  
The difference with `find_t0` is that this function _extrapolate_ the value of t₀ instead of interpolating.

### Returned value
The returned value is that `t̂` that solve the equation
        t2E(t) = refval
    
### Methodology
- _Fitting_: Those points in `t2E` in a given flow time range `[Tmin;Tmax]` are fitted to a parabola and coefficients are extracted. (default value are `Tmin=1.0` and `Tmax=4.0` )
- _Extrapolation_: Then, second order equation `ax² + bx + c = refval` is solved for `x=t₀`. Errors are handled by `ADerrors`

### Verbose mode
Verbose mode can be activated by setting `verbose=true` in the arguments. In that case
- Fitting coefficients are printed
- A plotting request is submitted to the author

## Missing
Sanity check has to be implemented.
This function works only for `uwreal` vectors.  

"""
function find_t0_extrapolate(time::Vector{Float64},t2E::Vector{uwreal},refval::Float64; delta=0.01,deltat=0.03125, Tmin=1.0,Tmax=4.0,t2Emax=0.12)
    # Apply cuts for the fitting range
    iicut = [tt for (tt,el) in enumerate(t2E) if ( deltat*tt>Tmin && deltat*tt<Tmax && el<t2Emax )]

    # Fit to function and extract t0
    @. parabola(x,par) = par[1]*x*x + par[2]*x + par[3]
    @. line(x,par) = par[1]*x + par[2]
    chisq(par,data) = sum( (data .- parabola(time[iicut],par)) .^2 ./ err.(t2E[iicut]) .^ 2 )

    # Fit with a parabola
    fit = curve_fit( parabola, time[iicut], value.(t2E[iicut]), [.5,.5,.5] )
    (fitpar,cexp) = fit_error(chisq,coef(fit),t2E[iicut])
    uwerr.(fitpar)

    # Fit with a line
    fitline = curve_fit( line, time[iicut], value.(t2E[iicut]), [.5,.5] )
    chisqline(par,data) = sum( (data .- line(time[iicut],par)) .^2 ./ err.(t2E[iicut]) .^ 2 )
    (fitparline,cexpline) = fit_error(chisqline,coef(fitline),t2E[iicut])
    uwerr.(fitparline)

    # Try to solve implicit equation
    solution = 0.0
    discriminant = fitpar[2]^2 - 4*fitpar[1]*(fitpar[3]-refval)
    try
        solution = (-fitpar[2] + sqrt(discriminant))/(2*fitpar[1])
        uwerr(solution)
    catch DomainError        
        if verbose
            println("Parabola coeffs are: $fitpar")
        end
        println("--- linear model used---")
        solution = (refval - fitparline[2])/fitparline[1]
        uwerr(solution)
        if verbose
            println("Coeffs are: $fitparline")
        end
    end
     
    return solution
end
function find_t0_extrapolate(time::Vector{Float64},t2E::Vector{Measurement{Float64}},refval::Float64; delta=0.01,deltat=0.03125, Tmin=1.0,Tmax=4.0,t2Emax=0.12)
    # Apply cuts for the fitting range
    iicut = [tt for (tt,el) in enumerate(t2E) if ( deltat*tt>Tmin && deltat*tt<Tmax && el<t2Emax )]

    # Fit to function and extract t0
    @. parabola(x,par) = par[1]*x*x + par[2]*x + par[3]
    @. line(x,par) = par[1]*x + par[2]
    chisq(par,data) = sum( (data .- parabola(time[iicut],par)) .^2 ./ err.(t2E[iicut]) .^ 2 )

    # Fit with a parabola
    fit = curve_fit( parabola, time[iicut], val.(t2E[iicut]), 1.0 ./ err.(t2E[iicut]) .^2, [.5,.5,.5] )
    fitpar = coef(fit) .± stderror(fit)
    
    # Fit with a line
    fitline = curve_fit( line, time[iicut], val.(t2E[iicut]), 1.0 ./ err.(t2E[iicut]) .^2,[.5,.5] )
    fitparline = coef(fitline) .± stderror(fitline)

    # Try to solve implicit equation
    solution = 0.0
    discriminant = fitpar[2]^2 - 4*fitpar[1]*(fitpar[3]-refval)
    try
        solution = (-fitpar[2] + sqrt(discriminant))/(2*fitpar[1])
    catch DomainError        
        println("--- linear model used---")
        solution = (refval - fitparline[2])/fitparline[1]
    end
     
    return solution
end




function find_t0_raw(time::Vector{Float64},t2E::Vector{Measurement{Float64}},refval::Float64; delta=0.001, Tmin=2.0,Tmax=5.0,deltat=0.03125)
    @. parabola(x,p) = p[1] + p[2]*x + p[3]*x^2
    @. line(x,p) = p[1] + p[2]*x

    iiok = [i for (i,(t,ϕ)) in enumerate(zip(time,t2E)) if (t>Tmin && t<Tmax) && (ϕ>refval-delta && ϕ<refval+delta)]

    fit = curve_fit(parabola,time[iiok],val.(t2E[iiok]), [.5,.5,.5] )
    (c,b,a) = coef(fit) .±  stderror(fit)

    s = (val(a)>0 ? -1 : 1)
    t0 = (-b +s*sqrt(b^2-4*a*(c-refval)))/2/a
    abs(val(t0))>10 ? t0 = (-b - s*sqrt(b^2-4*a*(c-refval)))/2/a : 0

    return t0
end







"""
    find_t0_NLO(time::Vector{Float64},t2E::Vector{uwreal},refval::Float64)

This function extract t0 by fitting `t2E` using perturbation theory at NLO.

"""
function find_t0_NLO(time::Vector{Float64},t2E::Vector{uwreal},ref::Float64,Nf::Float64; t0_guess=2.0)
    # Fix some constants
    b₀(nf) = (11. - 4. *nf)/3. /8. / π^2
    b₁(nf) = (34. - 32. *nf)/3. /128. / π^4
    alpha = 3. /128. / π^2
    b0 = b₀(Nf)
    a0 = b₁(Nf)/b₀(Nf)
    
    @. model(t,p) = -b0/2*log(t/p[1])

    # Define data to fit
    yfit = alpha./t2E .- alpha/ref .+ a0*log.(t2E./ref); 
    yfitval = [i.mean for i in yfit]
    yfiterr = err.(yfit)
    
    # Fit data
    p0 = [t0_guess]
    fit = curve_fit(model,time,yfitval,1.0 ./ yfiterr.^2 ,p0)
    chisq(p,d) = sum( (d .- model(time,p)).^2 ./ yfiterr.^2 )
    (fitp,χexp) = fit_error(chisq,coef(fit),yfit)
    uwerr.(fitp)

    return fit,fitp,χexp
end
function find_t0_NLO(time::Vector{Float64},t2E::Vector{Float64},ref::Float64,Nf::Float64; t0_guess=2.0)
    # Fix some constants
    b₀(nf) = (11. - 4. *nf)/3. /8. / π^2
    b₁(nf) = (34. - 32. *nf)/3. /128. / π^4
    alpha = 3. /128. / π^2
    b0 = b₀(Nf)
    a0 = b₁(Nf)/b₀(Nf)
    
    @. model(t,p) = -b0/2*log(t/p[1])

    # Define data to fit
    yfit = alpha./t2E .- alpha/ref .+ a0*log.(t2E./ref); 
    
    # Fit data
    p0 = [t0_guess]
    fit = curve_fit(model,time,yfit,p0)

    return fit
end


"""
    f_of_t(time::Vector{Float64},phi::Vector{uwreal},tref::Float64; delta=0.01)

This function gives the value of some function ϕ(t) at some given t.
"""
function phi_of_t(time::Vector{Float64},phi::Vector{uwreal},tref::Float64; delta=0.01)
    @. parabola(x,pars) = pars[1]*x^2 + pars[2]*x + pars[3]

    tokeep = [i for (i,flowt) in enumerate(time) if (flowt<tref+delta && flowt>tref-delta)]
    if length(tokeep)<3
        delta*=10
        tokeep = [i for (i,flowt) in enumerate(time) if (flowt<tref+delta && flowt>tref-delta)]
    end
    X = time[tokeep]
    Y = phi[tokeep] 

    fit = curve_fit(parabola,X,value.(Y),1.0 ./ err.(Y).^2, [0.5,0.5,0.5])

    chisq(p, d) = sum( (d .- parabola(X, p)) .^2 ./ err.(Y).^2)
    (fitp, cexp) = fit_error(chisq, coef(fit), Y)
    uwerr.(fitp)
    
    phi0 = fitp[1]*tref^2 + fitp[2]*tref + fitp[3]
    uwerr(phi0)

    return phi0
end


function cut(time,t2E,ref;deltat=0.03125)
    t1 = 0
    try
        for (i,ϕ) in enumerate(t2E)
            if val(ϕ) > ref
                t2 = time[i]; t1 = time[i-1]
                ϕ2 = ϕ;       ϕ1 = t2E[i-1]

                q = (t2*ϕ1 - t1*ϕ2)/deltat
                m = (ϕ2-ϕ1)/deltat        

                t1 = (ref - q)/m 
                break
            end
        end
    catch
        t1 = missing
    end
    return t1
end




















