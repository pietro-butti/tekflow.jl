"""
    PTfactor(N,x)

This function calculate correction factor for the flowed clover energy density according to
    
    `x⁴/128 ∑(μ!=ν)∑(q!=[0,0,0,0]) exp(-Nx²/4*q̂²)*( sin²(qᵤ) cos(qᵥ)² ) / q̂²   `

    where  `q̂` is the lattice momentum `q̂ᵤ = 2sin(q̂ᵤ/2)`
"""
function PTfactor(N,x)
    L = sqrt(N)
    Q    = 2. * π / L .* collect(0:L-1)
    hatQ = 2. .* sin.(Q./2.)

    S = 0.
    for μ in 1:4
        for ν in 1:4
            μ==ν ? continue : 0

            for mt in eachindex(Q)
            for mx in eachindex(Q)
            for my in eachindex(Q)
            for mz in eachindex(Q)
                (mt==1 && mx==1 && my==1 && mz==1) ? continue : 0

                @inbounds q     = [Q[mt],Q[mx],Q[my],Q[mz]]
                @inbounds hatq  = [hatQ[mt],hatQ[mx],hatQ[my],hatQ[mz]]
                hatq2 = sum(hatq.^2)

                @fastmath S += exp(-N*x^2/4. * hatq2) *
                        sin(q[ν])^2 *
                        cos(q[μ]/2.)^2 /
                        hatq2
            end
            end
            end
            end
        end
    end

    return S*x^4/128.
end


function PTfactor_generate_file(N::Int64,trange::StepRangeLen; path="_", filename="norm")
    p = (path=="_" ? "$(dirname(pathof(tekflow)))/PTnormalizations" : path)
    file = (filename=="norm" ? "$p/norm_n$(string(N)).dat" : filename)

    isfile(file) ? error("WARNING: $file already exist. Aborting...") : 0

    f = open(file,"w")
    @showprogress for t in trange
        x = sqrt(8*t/N)
        @printf(
            f,
            " %.8f        %.20f\n",
            t, PTfactor(N,x)
        )
    end
    close(f)

    println("Norm file written in $file for N=$N with t∈$trange")

    return
end


"""
    PTcorrect!(flow::TEKflow; path="src/PTnormalizations", filename="norm", usecols=2)

This function correct data contained in a `TEKflow struct` with the method explained in > [add reference]
The correction factor is read from a dat file and casted into a vector. 

### Correction factor file infos
## Format
File containing correction factors must be of the following format

    > more PTnormalizations/norm_n169.dat 
    # t                       norm_latt	                t            norm_cont
    0.000000000000000       0.000000000000000       0.00000000 0.00237471524161729 
    3.125000000000000E-002  1.762990553258006E-004	0.03125000 0.0023746824083981614381   
    6.250000000000000E-002  5.086584291421744E-004	0.06250000 0.0023746409628342950074   
    9.375000000000000E-002  8.457258415221814E-004	0.09375000 0.0023745566223751454063   
    
First column _must_ be the flow time, the correction factor could be in another column whose number can be set with the additional argument `usecols`

## Location
If nothing is specified, the function will read `flow.N` and read `PTnormalization/norm_<flow.N>.dat`.

Otw, user can set another flow file through additional arguments. For example, if the file is `/home/pippo/correctionfile.ext`, user has to set
- `path="/home/pippo/"`
- `filename="correctionfile.ext"`

## **Warning** 
Please, make sure that the time steps of the correction factor is the same used for the `flow` argument of the function. Otw, function will throw an error.
"""
function PTcorrect!(flow::TEKflow; path="_", filename="norm", usecols=2)
    path=="_" ? p = "$(dirname(pathof(tekflow)))/PTnormalizations" : p=path

    # Read norm correction factor
    if filename=="norm"
        file = "$p/norm_n$(string(flow.N)).dat"
    else
        file = "$p/$filename"
    end
    
    catfile = 0
    try
        catfile = readdlm(file,comments=true);
    catch
        error("Apparently $p/$filename is not a readable file. If it does not exist generate it with PTfactor_generate_file(N,trange).")
    end

    # Check if norm file have the same deltat
    deltat = catfile[2,1]-catfile[1,1]
    if deltat!=flow.deltat
        error("Δt is not compatible between flow and norm corrections.")
    end

    # Compute PT correction factor as 3/128*pi**2/Norm(t)
    Ninf = 3.0 / 128.0 / (π^2)
    NofT = Vector{Float64}(undef,flow.tmax)
    NofT[1] = 0.0
    for ii in 2:flow.tmax; NofT[ii] = Ninf/catfile[ii,usecols]; end

    # Correct t2E, t2Edata, t2Euwerr
    for tt in 1:flow.tmax
        flow.t2E[tt]        *= NofT[tt]
        flow.t2Edata[:,tt] .*= NofT[tt]
        flow.t2Euw[tt]      *= NofT[tt]
    end

    uwerr.(flow.t2Euw)

    if flow.flag_PTcorrect_has_been_called
        error("WARINING: PTcorrect! has already been called on this TEKflow object. Returning 1...")
    else
        flow.flag_PTcorrect_has_been_called = true
    end

    return 0
end
function PTcorrect!(flow::TEKflowav; path="_", filename="norm", usecols=2)
    path=="_" ? p = "$(dirname(pathof(tekflow)))/PTnormalizations" : p=path

    # Read norm correction factor
    if filename=="norm"
        file = "$p/norm_n$(string(flow.N)).dat"
    else
        file = "$p/$filename"
    end
    
    catfile = 0
    try
        catfile = readdlm(file,comments=true);
    catch
        error("Apparently $p/$filename is not a readable file. If it does not exist generate it with PTfactor_generate_file(N,trange).")
    end

    # Check if norm file have the same deltat
    deltat = catfile[2,1]-catfile[1,1]
    if deltat!=flow.deltat
        error("Δt is not compatible between flow and norm corrections.")
    end

    # Compute PT correction factor as 3/128*pi**2/Norm(t)
    Ninf = 3.0 / 128.0 / (π^2)
    NofT = Vector{Float64}(undef,flow.tmax)
    NofT[1] = 0.0
    for ii in 2:flow.tmax; NofT[ii] = Ninf/catfile[ii,usecols]; end

    # Correct t2E, t2Edata, t2Euwerr
    for tt in 1:flow.tmax
        flow.t2E[tt]        *= NofT[tt]
        flow.t2E_std[tt]    *= NofT[tt]
        flow.t2Euw[tt]      *= NofT[tt]
    end

    if flow.flag_PTcorrect_has_been_called
        error("WARINING: PTcorrect! has already been called on this TEKflow object. Returning 1...")
    else
        flow.flag_PTcorrect_has_been_called = true
    end

    return 0
end


function find_t0_PTcuts(flow::TEKflow,refval::Float64;Tmin=1.0,Tmax=Float64(flow.N/128.0),t2Emax=0.12, delta=0.01, verbose=false)
    # Apply cuts
    iicut = [tt for (tt,el) in enumerate(flow.t2E) if ( flow.deltat*tt>Tmin && flow.deltat*tt<Tmax && el<t2Emax )]

    # Fit to function and extract t0
    @. parabola(x,par) = par[1]*x*x + par[2]*x + par[3]
    @. line(x,par) = par[1]*x + par[2]
    chisq(par,data) = sum( (data .- parabola(flow.time[iicut],par)) .^2 ./ err.(flow.t2Euw) .^ 2 )


    # Fit with a parabola
    # fit = curve_fit( parabola, flow.time[iicut], value.(flow.t2Euw[iicut]), 1.0 ./ err.(flow.t2Euw[iicut]) .^ 2, [.5,.5,.5] )
    fit = curve_fit( parabola, flow.time[iicut], value.(flow.t2Euw[iicut]), [.5,.5,.5] )
    (fitpar,cexp) = fit_error(chisq,coef(fit),flow.t2Euw[iicut])
    uwerr.(fitpar)

    # Check if t0 is ax^2 + bx + c = refval has solutions
    discriminant = fitpar[2]^2 - 4*fitpar[1]*(fitpar[3]-refval)
    if discriminant>0
        solution = (-fitpar[2] + sqrt(discriminant))/(2*fitpar[1])
        uwerr(solution)

        if verbose
            println("Verbose mode ON:")
            println("Coefficients are: ")
            println.(fitpar)

            println("Do you want me to plot it? (y/n)")
            answer = readline()
            if answer=="y"
                axvline(x=Tmin,color="gray")
                axvline(x=Tmax,color="gray")
                axhline(y=refval,color="gray",alpha=0.3)
                plot(flow.time,parabola(flow.time,coef(fit)),color="C1")
                fill_between(flow.time,flow.t2E+err.(flow.t2Euw),flow.t2E-err.(flow.t2Euw),alpha=0.5,color="C0")
                show()
            end
        end


        return solution

    else
        if verbose   
            println("Verbose mode ON:")
            print("WARNING: Fitted parabola does not let t0 to be extracted. Can I show you? (y/n) ")
            answer = readline()
            if answer=="y"
                axvline(x=Tmin,color="gray")
                axvline(x=Tmax,color="gray")
                plot(flow.time,parabola(flow.time,coef(fit)),color="C1")
                fill_between(flow.time,flow.t2E+err.(flow.t2Euw),flow.t2E-err.(flow.t2Euw),alpha=0.5,color="C0")
                println("What to do now?")
            elseif answer=="n"
                println("Ok boomer, what to do now then?")
                return 0
            end
        end

        # println("Do you want me to use linear model to fit? (y/n) ")
        # answer2 = readline()
        println("Parabola bends downward, I am using a linear model")
        answer2=="y"
        if answer2=="y"
            chisq2(par,data) = sum( (data .- line(flow.time[iicut],par)) .^2 ./ 0.01 .^ 2 )

            fit2 = curve_fit( line, flow.time[iicut], value.(flow.t2Euw[iicut]), 1.0 ./ err.(flow.t2Euw[iicut]) .^ 2, [.5,.5] )
            (fitpar2,cexp) = fit_error(chisq2,coef(fit2),flow.t2Euw[iicut])
            t0 = (refval-fitpar2[2])/fitpar2[1]

            if verbose  
                println("Verbose mode ON:")
                print("Can I show you? (y/n) ")
                answer = readline()
                if answer=="y"
                    plot(flow.time,line(flow.time,coef(fit2)),color="C2")
                    axhline(y=refval)
                    axvline(x=value(t0))
                    show()
                elseif answer=="n"
                    println("Ok boomer, what to do now then?")
                end
            end

            return t0
        else
            println("Ok boomer, then returning 0...")
            return 0
        end
    end

    return

end