"""
    multicol(flow::TEKflow,usecols::Vector{Int64})

This function collect datas from TEK flow simulations taking several columns, given a TEKflow object

### Arguments:
- `flow::TEKflow`: a TEKflow object which has already been istantiated. It cannot be taken from a `.npy` file, but from flowjob files in a folder.
- `usecols::Vector{Int64}`: a vector containing the numbers of the columns one wants to read from the flowjob file.

### Returned
- `flow.multi::Vector{Vector{uwreal}}` is filled. It is a 2 index structure χ[R][t]
- `flow.multidata::Array{Float64,3}` is filled. It is a 3 index structure χ[R,t,conf]
"""
function multicol(flow::TEKflow,usecols::Vector{Int64})
    flow.flag_multicol_has_been_called = true

    path = "$(flow.path)/$(flow.basement)/" 
    if length(readdir(path,join=true))!=flow.Nconf; println("LOCAL ERROR: Is $path the same used to instantiate $flow?"); return -1; end;
        
    flow.multidata = Array{Float64}(undef,length(usecols),flow.tmax,flow.Nconf)
    
    # Harvest data ---------------------------------------
    for (conf,file) in enumerate(readdir(path,join=true))
        flowraw = 0
        try 
            flowraw = readdlm(file,comments=true)
        catch
            println("LOCAL ERROR: $file is not readable by readdlm. Skipping it...")
        continue
    end

    # Store data in a matrix ------------------------------
    if flow.time[1]!=flowraw[1,1]
        for (icol,col) in enumerate(usecols)
            for (it,tt) in enumerate(flow.time)
                if it==1
                    flow.multidata[icol,it,conf] = flowraw[1,col]
                else
                    flow.multidata[icol,it,conf] = flowraw[it-1,col]
                end
            end
        end
    else
        for (icol,col) in enumerate(usecols)
            for (it,tt) in enumerate(flow.time)
                flow.multidata[icol,it,conf] = flowraw[it,col]
            end
        end
    end
    end

    # Produce a uwreal matrix χs[R,t] ---------------------------------
    flow.multi = Vector{Vector}(undef,length(usecols))
    for (icol,col) in enumerate(usecols)
        flow.multi[icol] = Vector{uwreal}(undef,length(flow.time))
        for (it,tt) in enumerate(flow.time)
            flow.multi[icol][it] = uwreal(flow.multidata[icol,it,:],ensembles(flow.t2Euw[it])[1])
        end
        uwerr.(flow.multi[icol])
    end

    return 0
end
