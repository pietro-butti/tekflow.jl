"""
    Latest version: July 22th, 2022
"""
module tekflow

    using Base: Int64, Float64, Float16
    using DelimitedFiles,Statistics, ADerrors, PyPlot, LsqFit, PyCall, Printf, Optim, ProgressMeter, Measurements, Polynomials

    val(u::uwreal) = u.mean
    val(u::Measurement) = u.val
    val(u::Float64) = u

    err(u::uwreal) = u.err
    err(u::Measurement) = u.err

    Base.abs(uw::uwreal) = Base.abs(uw.mean)

    include("tekflow_types.jl")
    include("tekflow_creutz.jl")
    include("tekflow_t0.jl")
    include("tekflow_normPT.jl")
    include("tekflow_scale.jl")
    include("tekflow_plot.jl")
    include("tekflow_tools.jl")
    include("tekflow_ddt.jl")

    export val, err
    export TEKflow, TEKflowav
    export multicol
    export find_t0_interpolate, find_t0_extrapolate, find_t0_raw, find_t0_NLO, phi_of_t, cut
    export PTfactor, PTfactor_generate_file, PTcorrect!, find_t0_PTcuts
    export PTconstants, fit_parameters, flat_data, TEKtensor, flat_PTfit, flat_fit_errors, flat_polyfit
    export flowplot, scatterplot, behold
    export rebin, average, average_creutz
    export tddt_flow

end