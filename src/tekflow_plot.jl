function flowplot(time::Vector{Float64},flow::Vector{T};Tmin=0.0,Tmax=-1.0,kwargs...) where T
    if length(time)!=length(flow)
        error("LOCAL ERROR: X and Y are not of the same length. Returning -1...")
    end

    T==uwreal ? uwerr.(flow) : 0
    deltat = time[2]-time[1]

    if (typeof(Tmin)==Float64 && typeof(Tmax)==Float64)
        if (Tmax==-1.0 && Tmin==0.0)
            tmin = 1
            tmax = length(time)
        elseif (Tmax==-1.0 && Tmin!=0.0)
            tmin = trunc(Int,Tmin/deltat)
            tmax = length(time)
        elseif (Tmax!=-1.0 && Tmin==0.0)
            tmin = 1
            tmax = trunc(Int,Tmax/deltat) 
        else
            tmin = trunc(Int,Tmin/deltat)
            tmax = trunc(Int,Tmax/deltat) 
        end  
    elseif (typeof(Tmin)==Int64 && typeof(Tmax)==Float64)
        tmin = Tmin
        tmax = trunc(Int,Tmax/deltat)
    elseif (typeof(Tmin)==Float64 && typeof(Tmax)==Int64)
        tmin = trunc(Int,Tmin/deltat)
        if tmin==0; tmin=1; end
        tmax = Tmax
    elseif (typeof(Tmin)==Int64 && typeof(Tmax)==Int64)
        tmin = Tmin
        tmax = Tmax
    end
    iplot = collect(Int64,tmin:tmax)

    X = time[iplot]
    # Yp = value.(flow[iplot]) .+ err.(flow[iplot])
    # Ym = value.(flow[iplot]) .- err.(flow[iplot]) 
    Yp = val.(flow[iplot]) .+ err.(flow[iplot])
    Ym = val.(flow[iplot]) .- err.(flow[iplot])    

    pycall(plt.fill_between,PyAny,X,Yp,Ym;kwargs...)

    return 0
end
function flowplot(time::Vector{Float64},flow::Vector{Float64}; Tmin=0.0, Tmax=-1.0,yerr=[],kwargs...)
    if length(time)!=length(flow)
        error("LOCAL ERROR: X and Y are not of the same length. Returning -1...")
    end

    deltat = time[2]-time[1]
    
    if (typeof(Tmin)==Float64 && typeof(Tmax)==Float64)
        if (Tmax==-1.0 && Tmin==0.0)
            tmin = 1
            tmax = length(time)
        elseif (Tmax==-1.0 && Tmin!=0.0)
            tmin = trunc(Int,Tmin/deltat)
            tmax = length(time)
        elseif (Tmax!=-1.0 && Tmin==0.0)
            tmin = 1
            tmax = trunc(Int,Tmax/deltat) 
        else
            tmin = trunc(Int,Tmin/deltat)
            tmax = trunc(Int,Tmax/deltat) 
        end  
    elseif (typeof(Tmin)==Int64 && typeof(Tmax)==Float64)
        tmin = Tmin
        tmax = trunc(Int,Tmax/deltat)
    elseif (typeof(Tmin)==Float64 && typeof(Tmax)==Int64)
        tmin = trunc(Int,Tmin/deltat)
        if tmin==0; tmin=1; end
        tmax = Tmax
    elseif (typeof(Tmin)==Int64 && typeof(Tmax)==Int64)
        tmin = Tmin
        tmax = Tmax
    end
    iplot = collect(Int64,tmin:tmax)

    if yerr==[]
        pycall(plt.plot,PyAny,time[iplot],flow[iplot];kwargs...)
    else
        if (typeof(yerr)==Vector{Float64} && length(yerr)==length(flow))
            pycall(ply.fill_between,PyAny,time[iplot],flow[iplot].+yerr[iplot],flow[iplot].-yerr[iplot];kwargs...)
        end
    end

    return 0
end
function flowplot(flow::Vector{T}; Tmin=1, Tmax=-1, yerr=[], kwargs...) where T
    if typeof(Tmin)!=Int64 || typeof(Tmax)!=Int64
        error("Give a legal type to Tmin or Tmax\n")
    end

    yplot = flow[Tmin:Tmax]
    xplot = [i for i in 1:length(yplot)]

    if T==uwreal
        pycall(plt.fill_between,PyAny,xplot,value.(yplot).+err.(yplot),value.(yplot).-err.(yplot);kwargs...)
    elseif T==Float64
        pycall(plt.plot,PyAny,xplot,yplot;kwargs...)
    end

    return 0 
end


function scatterplot(xx::Vector{T}, yy::Vector{Terr}; kwargs...) where T<:Number where Terr
    y    = Vector{Float64}(undef,length(yy))
    erry = Vector{Float64}(undef,length(yy))
    try
        y    = val.(yy)
        erry = err.(yy)
    catch MethodError
        error("DECLARATION ERROR: val and/or err are not defined for the type $Terr")
    end

    pycall(plt.errorbar,PyAny,xx,y,yerr=erry; kwargs...)
    return 0
end
function scatterplot(xx::Vector{Terr}, yy::Vector{T}; kwargs...) where T<:Number where Terr
    x    = Vector{Float64}(undef,length(xx))
    errx = Vector{Float64}(undef,length(xx))
    try
        x    = val.(xx)
        errx = err.(xx)
    catch MethodError
        error("DECLARATION ERROR: val and/or err are not defined for the type $Terr")
    end

    pycall(plt.errorbar,PyAny,x,yy,xerr=errx;kwargs...)
    return 0
end
function scatterplot(xx::Vector{Terr}, yy::Vector{Terr}; kwargs...) where Terr
    x    = Vector{Float64}(undef,length(xx))
    errx = Vector{Float64}(undef,length(xx))
    y    = Vector{Float64}(undef,length(yy))
    erry = Vector{Float64}(undef,length(yy))
    try
        x    = val.(xx)
        errx = err.(xx)
        y    = val.(yy)
        erry = err.(yy)
    catch MethodError
        error("DECLARATION ERROR: val and/or err are not defined for the type $Terr")
    end

    pycall(plt.errorbar,PyAny,x,y,yerr=erry,xerr=errx;kwargs...)
    return 0
end




function behold(;loc=".",saveto=".")
    grid()
    loc=="." ? legend() : legend(loc=loc)
    saveto=="." ? display(gcf()) : savefig(saveto)
    close()
    return 0
end