"""
    TEKflow(basement::String; path=".", deltat=0.03125, usecols=4)

PREREQUISITES:
Flow files has to be like:

    #     epsilon=0.0312500000
    #  int. order=           3
    #       niter=         320
    #          ng=         289
    #         nsq=          17
    #         nqt=           5
    #t             (1-plaq)          t^2E [plaq]           t^2E [symm]
    0.0000000000  0.4600378684E+00  0.0000000000E+00  0.0000000000E+00
    0.0312500000  0.3713895197E+00  0.4352220935E-02  0.6416403887E-03
    0.0625000000  0.2965333554E+00  0.1390000103E-01  0.2426343155E-02
    0.0937500000  0.2352684692E+00  0.2481347136E-01  0.5070791158E-02
    0.1250000000  0.1862710136E+00  0.3492581505E-01  0.8244142918E-02
    ...

and they have to be stored in a folder called with the name of the ensemble e.g. "n289b0350k5hf1775"
 
FUNCTIONALITIES:
- to be instantiated, it only needs the string with the ensemble name
    `flow = TEKflow("n289b0350k5hf1875")`
From that string, when possible, it will read (N,b,kappa) callable as 
    `this.N, this.b, this.kappa`
when not possible, they will be set to default values (-1,-1,-1)

- from each flow file, the 4th column is stored in a Matrix{Float64} callable as
    `flow.t2Edata`
where indexing is done such that this.t2Edata[configuration,flowtime]

- Average value and standard deviation errors are Vector{Float64} callable as
    `this.t2E`
    `this.t2E_std`
"""
mutable struct TEKflow
    ################################ ATTRIBUTES ####################################
    path::String
    basement::String

    # Ensemble parameters
    N::Int64      
    b::Float64    
    kappa::Float64
    Nconf::Int64

    # Data structure
    t2E::Vector{Float64}
    t2E_std::Vector{Float64}
    t2Edata::Matrix{Float64}
    t2Euw::Vector{uwreal}
    time::Vector{Float64}
    tmax::Int64
    
    # Optional parameters
    deltat::Float64
    usecols::Int
    
    # Flag function
    flag_multicol_has_been_called::Bool
        multidata::Array{Float64,3}
        multi::Vector{Vector{uwreal}}
    flag_PTcorrect_has_been_called::Bool
    flag_tddt_flow_has_been_called::Bool
        wt2E::Vector{Float64}
        wt2Edata::Matrix{Float64}
        wt2Euw::Vector{uwreal}
    
    
    ############################## CONSTRUCTORS #####################################
    function TEKflow(basement::String; path=".", deltat=0.03125, usecols=4, verbose=true)
        
        this = new();
        # Default attribute initialization =====================================
        this.basement  = basement
        this.path      = path
        this.deltat    = deltat
        this.usecols   = usecols

        this.flag_PTcorrect_has_been_called = false
        this.flag_tddt_flow_has_been_called = false
        
        try
            this.N = parse(Int64,basement[2:4])
        catch
            this.N = -1
        end
        try
            this.b = parse(Float64,this.basement[6:9])/1000
        catch
            this.b = - .1
        end
        try
            ndigits = length(this.basement[14:end])
            this.kappa = parse(Float64,this.basement[14:end])/10^ndigits
        catch
            this.kappa = - .1
        end
        
        # Harvest data ==========================================================
        path = "$path/$basement/" 
        len  = Vector{Int}()
        aux  = Vector{Vector{Float64}}()
        if isfile("$(path[1:end-1]).npy")
                np = pyimport("numpy")
                aux = np.load("$(path[1:end-1]).npy")
                len = fill(size(aux)[2],3)
        elseif isdir(path)
            for file in readdir(path,join=true)
                flowraw = 0
                try
                    if filesize(file)!=0
                        flowraw = readdlm(file,comments=true)
                    else
                        verbose ? println("READING ERROR: $file is empty. Skipping it") : 0
                    end
                catch
                    verbose ? println("READING ERROR: $file is not readable by readdlm. Skipping it...") : 0
                    continue
                end
                
                tt=0; flow=0
                try
                    tt  = flowraw[:,1]
                    flow = flowraw[:,usecols]
                    # Check if first t=0.0 --------------
                    if tt[1]!=0.
                        pushfirst!(tt,0.0)
                        pushfirst!(flow,0.0)
                    end # -------------------------------
                    
                    # Append ----------------------------
                    push!(aux,flow)
                    push!(len,length(tt))
                    # -----------------------------------
                catch
                    println("INPUT ERROR: $file is weird, check it. Skipping it...")
                    continue
                end
            end
        else
            println("INPUT ERROR: $path is neither a directory nor a .npy file. Returning -1...")
            return -1
        end
       


        # Cast data into a matrix ===============================================
        this.tmax    = min(len...)
        this.Nconf   = size(aux)[1]

        this.t2Edata = Matrix{Float64}(undef,this.Nconf,this.tmax)
        for conf in 1:this.Nconf
            try
                this.t2Edata[conf,:] = aux[conf][1:this.tmax]
            catch
                this.t2Edata[conf,:] = aux[conf,1:this.tmax]
            end
        end
        
        this.time    = Vector{Float64}(undef,this.tmax)
        this.t2E     = Vector{Float64}(undef,this.tmax)        
        this.t2E_std = Vector{Float64}(undef,this.tmax) 
        this.t2Euw   = Vector{uwreal}(undef,this.tmax)       
        for tt in 1:this.tmax
            this.time[tt]    = this.deltat*Float64(tt-1)
            this.t2E[tt]     = mean(this.t2Edata[:,tt]) 
            this.t2E_std[tt] = std(this.t2Edata[:,tt])/sqrt(this.Nconf) 
            this.t2Euw[tt]   = uwreal(this.t2Edata[:,tt],"$(this.basement) flow data")
        end

        # Call errors
        uwerr.(this.t2Euw)


        verbose ? println("TEKflow object has been instantiated. Have fun!") : 0
        return this
    end
end
function Base.show(io::IO, a::TEKflow)
    println(io,"Wilson flow data for $(a.basement) (N=$(a.N), b=$(a.b), κ=$(a.kappa), Nconf=$(a.Nconf))")
    return
end

function format!(flow::TEKflow,tmax::Int64)
    if tmax<flow.tmax
        flow.tmax = tmax
        flow.time[:]  = flow.time[1:tmax]
        flow.t2E[:]   = flow.t2E[1:tmax]
        flow.t2Euw[:] = flow.t2Euw[1:tmax]
    end
    return
end



mutable struct TEKflowav
    path::String
    basement::String

    # Ensemble parameters
    N::Int64      
    b::Float64    
    kappa::Float64

    # Data structure
    t2E::Vector{Float64}
    t2E_std::Vector{Float64}
    t2Euw::Vector{Measurement{Float64}}
    time::Vector{Float64}
    tmax::Int64
    
    # Optional parameters
    deltat::Float64
    usecols::Vector{Int64}
    
    # Flag function
    flag_PTcorrect_has_been_called::Bool
    flag_tddt_flow_has_been_called::Bool
        wt2E::Vector{Float64}
        wt2Edata::Matrix{Float64}
        wt2Euw::Vector{Float64}
    
    ############################## CONSTRUCTORS #####################################
    function TEKflowav(filename::String; path=".", deltat=0.03125, usecols=[2,3], verbose=true)
        this = new();

        # Default attribute initialization =====================================
        this.basement  = split(filename,".")[1]
        this.path      = path
        this.deltat    = deltat
        this.usecols   = usecols

        this.flag_PTcorrect_has_been_called = false
        this.flag_tddt_flow_has_been_called = false
        
        try
            this.N = parse(Int64,this.basement[2:4])
        catch
            this.N = -1
        end
        try
            this.b = parse(Float64,this.basement[6:9])/1000
        catch
            this.b = - .1
        end
        try
            ndigits = length(this.basement[14:end])
            this.kappa = parse(Float64,this.basement[14:end])/10^ndigits
        catch
            this.kappa = - .1
        end


        # Harvest data
        toread = joinpath(path,filename)
        isfile(toread) ? 0 : error("READING ERROR: $toread is not a readable file.")
        catfile = readdlm(toread,comments=true)
        
        this.tmax = size(catfile,1)

        this.time     = catfile[:,1]
        this.t2E      = catfile[:,usecols[1]]
        this.t2E_std  = catfile[:,usecols[2]]
        this.t2Euw    = this.t2E .± this.t2E_std

        verbose ? println("TEKflow object has been instantiated. Have fun!") : 0
        return this
    end
end
function Base.show(io::IO, a::TEKflowav)
println(io,"Wilson flow average for $(a.basement) (N=$(a.N), b=$(a.b), κ=$(a.kappa))")
return
end