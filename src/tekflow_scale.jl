mutable struct PTconstants
    Nf::Float64
    
    alpha::Float64
    b0::Float64
    b1::Float64
    β::Function

    function PTconstants(nf::Float64)
        this = new(nf)

        this.alpha = (3. /128. /π^2)
        this.b0 = (11. - 4. *nf)/3. /8. / π^2
        this.b1 = (34. - 32. *nf)/3. /128. / π^4
        this.β  = (λ,p)  -> -this.b0*λ^2 / (1 - this.b1/this.b0*λ)
        return this
    end
end
function Base.show(io::IO, a::PTconstants)
    println(io,"Constants for global fitting with PT@NLO for Nf=$(a.Nf)")
    return
end


struct fit_parameters
    Tmin::Float64
    gamma::Float64
    t2Emax::Float64
    Npars::Int64
    tmax::Function
    fit_parameters(t,g,e,n) = new(t,g,e,n,Ncol->g^2*Ncol/8,)
end
function Base.show(io::IO, a::fit_parameters)
    println(io,"(Tmin=$(a.Tmin), γ=$(a.gamma), Npars=$(a.Npars))")
    return
end



mutable struct flat_data
    flat_time::Vector{Float64}
    flat_ϕ::Vector{uwreal}
    flat_iifit::Vector{Int64}
    flat_t0ii::Vector{Int64}
end

mutable struct TEKtensor
    flows::Vector{TEKflow}
    pt::PTconstants
    pars::fit_parameters
    
    Nsets::Int64
    flatd::flat_data
    
    chunkii::Vector{Vector{Int64}}
    
    function TEKtensor(fls,pts,pps)
        this = new(fls,pts,pps)
        
        this.Nsets = length(fls)
        this.chunkii = Vector{Vector{Int64}}(undef,this.Nsets)

        # Crop flow data
        times = Vector{Vector{Float64}}(undef,this.Nsets)
           ϕs = Vector{Vector{uwreal}}(undef,this.Nsets)
         fitr = Vector{Vector{Int64}}(undef,this.Nsets)
         t0ii = Vector{Vector{Int64}}(undef,this.Nsets)
        for (ii,flow) in enumerate(fls)
            Ncol = flow.N
            iifit = [i for (i,(t,f)) in enumerate(zip(flow.time,flow.t2E)) if t>pps.Tmin && t<pps.tmax(Ncol) && f<pps.t2Emax]
        
            times[ii] = flow.time[iifit]
               ϕs[ii] = flow.t2Euw[iifit]
             fitr[ii] = iifit
             t0ii[ii] = fill(ii,length(iifit))

            this.chunkii[ii] = (ii==1 ? collect(1:length(iifit)) : (collect(1:length(iifit)) .+ this.chunkii[ii-1][end]))
        end

        # Flatten data
        flat_time  = vcat(times...)
        flat_ϕ     = vcat(ϕs...)
        flat_iifit = vcat(fitr...)
        flat_t0ii  = vcat(t0ii...)

        this.flatd = flat_data(flat_time,flat_ϕ,flat_iifit,flat_t0ii)
    
        return this
    end
end
function Base.show(io::IO, a::TEKtensor)
    println(io,"Cropped flow data.")
    for flow in a.flows
        println(io,"$(flow.basement)")
    end
    print(io,"Total dof = (Npoints=$(length(a.flatd.flat_time)))-(Nsets=$(a.Nsets))-(Npars=$(a.pars.Npars)) = $(length(a.flatd.flat_time)-a.Nsets-a.pars.Npars)")
    return
end


function flat_polyfit(ϕ::TEKtensor,p0,ref;verbose=true)
    length(p0) == ϕ.Nsets+ϕ.pars.Npars ? 0 : error("Fix guess parameters")

    xfit = ϕ.flatd.flat_time
    yfit = val.(ϕ.flatd.flat_ϕ)
    erry = tekflow.err.(ϕ.flatd.flat_ϕ)

    polynomial(x,p) = Polynomial(vcat(ref.-sum(p[1:ϕ.pars.Npars]),p[1:ϕ.pars.Npars]))(x/p[end])

    function flat_poly(fdata,p)
        aux = deepcopy(fdata)
        for (i,f) in enumerate(fdata)
            pars = vcat(p[1:ϕ.pars.Npars],p[ϕ.pars.Npars+ϕ.flatd.flat_t0ii[i]])
            aux[i] = polynomial(f,pars)
        end
        return aux
    end
    
    fit = curve_fit(flat_poly,xfit,yfit,1.0 ./ erry.^2,p0)

    if verbose
        parameters = coef(fit)
        println(
        "--------- FIT RESULTS (w/ stat. error) --------- \n",
        "PT coefficients:",
        )
        [println("                 $(parameters[k])") for k in 1:ϕ.pars.Npars];
        println("                     t₁,                      sqrt(8t₁):")
        [
            @printf(
                "%.17s => %.8f           %.8f \n",
                ϕ.flows[i].basement,
                parameters[ϕ.pars.Npars+i],
                sqrt(8*parameters[ϕ.pars.Npars+i]),
            )
            for i in 1:ϕ.Nsets
        ];
        println(
            "\n",
            "χ²/(Ndof=$(dof(fit))) = $(round(sum(fit.resid.^2)/dof(fit),digits=3))\n",
            "---------------------------------------------"
        )
    end

    return flat_poly, fit
end



function flat_PTfit(ϕ::TEKtensor,p0,ref;verbose=true)
    length(p0) == ϕ.Nsets+ϕ.pars.Npars ? 0 : error("Fix guess parameters")

    xfit = val.(ϕ.flatd.flat_ϕ)
    yfit = ϕ.pt.b0/2 .* log.(ϕ.flatd.flat_time)
    
    function flat_model(fdata,p)
        aux = deepcopy(fdata)
        for (i,f) in enumerate(fdata)
            aux[i] = - ϕ.pt.alpha*(1/f - 1/ref) -
                    ϕ.pt.b1/ϕ.pt.b0*log(f/ref) -
                    sum([p[k]/k*((f/ϕ.pt.alpha)^k - (ref/ϕ.pt.alpha)^k) for k in 1:ϕ.pars.Npars]) + 
                    ϕ.pt.b0/2*p[ϕ.pars.Npars+ϕ.flatd.flat_t0ii[i]]
        end
        return aux
    end
    
    erry = (ϕ.pt.alpha ./ xfit.^2 .- ϕ.pt.b1/ϕ.pt.b0 ./ xfit).*err.(ϕ.flatd.flat_ϕ)
    fit = curve_fit(
        flat_model,
        xfit,
        yfit,
        1.0 ./ erry.^2,
        p0
    )

    if verbose
        parameters = coef(fit) .± stderror(fit)
        println(
        "--------- FIT RESULTS (w/ stat. error) --------- \n",
        "PT coefficients:",
        )
        [println("                 $(parameters[k])") for k in 1:ϕ.pars.Npars];
        println("                     t₁,                      sqrt(8t₁):")
        [
            @printf(
                "%.17s => %.8f ± %.8f           %.8f ± %.8f \n",
                ϕ.flows[i].basement,
                val(exp(parameters[ϕ.pars.Npars+i])),
                err(exp(parameters[ϕ.pars.Npars+i])),
                val(sqrt(8*exp(parameters[ϕ.pars.Npars+i]))),
                err(sqrt(8*exp(parameters[ϕ.pars.Npars+i])))
            )
            for i in 1:ϕ.Nsets
        ];
        println(
            "\n",
            "χ²/(Ndof=$(dof(fit))) = $(round(sum(fit.resid.^2)/dof(fit),digits=3))\n",
            "---------------------------------------------"
        )
    end

    return flat_model, fit
end





function flat_fit_errors(ϕ::TEKtensor, fit::LsqFit.LsqFitResult, ref::Float64 ; verbose=true)
    beta(λ) = - ϕ.pt.b0*λ^2 / (1. - ϕ.pt.b1/ϕ.pt.b0*λ - sum([coef(fit)[k]*λ^(k+1) for k in 1:ϕ.pars.Npars]))
    
    δλ = err.(ϕ.flatd.flat_ϕ)./ϕ.pt.alpha
    function cost(p,data)
        Hₜ = [
            ϕ.pt.alpha*(1/f - 1/ref) - 
            ϕ.pt.b1/ϕ.pt.b0 * log(f/ref) + 
            sum([p[k]/k/ϕ.pt.alpha^k*(f^k - ref^k) for k in 1:ϕ.pars.Npars]) + 
            ϕ.pt.b0/2 * log(ϕ.flatd.flat_time[i]) - ϕ.pt.b0/2 * p[ϕ.pars.Npars + ϕ.flatd.flat_t0ii[i]]
            for (i,f) in enumerate(data)
        ]
        δHₜ = ϕ.pt.b0 ./ δλ ./ abs.( beta.(data) )
        return sum((Hₜ./δHₜ).^2)
    end

    (fitp,χexp) = fit_error(cost,coef(fit),ϕ.flatd.flat_ϕ)
    uwerr.(fitp)

    χ2 = cost(coef(fit),ϕ.flatd.flat_ϕ)

    t1s = exp.(fitp[ϕ.pars.Npars+1:end]); uwerr.(t1s)
    st1 = sqrt.(8 .* t1s); uwerr.(st1)

    if verbose
        println(
            "--------- FIT RESULTS (w/ ADerrors) --------- \n",
            "PT coefficients:",
        )
        [println(
            "                 $(fitp[k])"
        ) for k in 1:ϕ.pars.Npars];
        println("                     t₁,              sqrt(8t₁):")
        [
            @printf(
                "%.17s => %.8f ± %.8f           %.8f ± %.8f \n",
                ϕ.flows[i].basement,
                val(t1s[i]),
                err(t1s[i]),
                val(st1[i]),
                err(st1[i])
            )
            for i in 1:ϕ.Nsets
        ];
        println(
            "χ²=$(val(χ2)),     χ²/χ²exp=$(val(χ2/χexp))"
        )
    end

    return cost, fitp, χexp
end    